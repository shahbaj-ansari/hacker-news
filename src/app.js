const urlFrontPage = "https://hn.algolia.com/api/v1/search?tags=front_page";

// Initially Fetch first 20 posts
fetchPosts(urlFrontPage);

// Text From Search a Blog
const searchPost=document.querySelector('#searchBlog');

function HighlightPattern(){
    
    const matchedpattern=document.querySelectorAll('h4');
    const pattern=searchPost.value;
    const patternRegex=new RegExp(`${pattern}`,'gi');

    if(pattern==undefined || pattern==''){
        return;
    }

    for(let i=matchedpattern.length-1;i>=0;i--){
        matchedpattern[i].innerHTML=matchedpattern[i].innerHTML.replace(patternRegex,`<mark>${pattern}</mark>`);
    }
    
    const matchedLink=document.querySelectorAll('.blogIntro a');

    for(let i=matchedLink.length-1;i>=0;i--){
        matchedLink[i].innerHTML=matchedLink[i].innerHTML.replace(patternRegex,`<mark>${pattern}</mark>`);
    }

}

function fetchPosts(url,HighlightPattern){
    
   // console.log(`fetching data for: ${url}`);
    
    const blogs = document.querySelector("div.posts");
    blogs.innerHTML="";
    blogs.setAttribute('class','posts loader');
    
    fetch(url)
	.then((response) => response.json())
	.then((data) => {
    
        const defaultSize=20; // urlFrontPage returns 20 post for HomePage
        blogs.innerHTML="";
        const fragment=document.createDocumentFragment();

		for (let i = 0; i < defaultSize; i++) {

			let newPost = document.createElement("div");
			const post = data["hits"][i];
			// console.log(post);
            const link=`<a href=${post.url}>${post.url}</a>`;

			const newPostContent= `
                <div class=single-post>
                    <div class="blogIntro">
                        <h4> <a href=${post.url}> ${post.title} </a> </h4>
                        <p>(${link})</p>
                    </div>
                    <div class="blogInfo">
                        <p> <a href=${post.url}> ${post.points} points | </a> </p>
                        <p> <a href=${post.url}> ${post.author} |</a> </p>
                        <p> <a href=${post.url}> ${moment(post.created_at).fromNow()} | </a> </p>
                        <p> <a href=${post.url}> ${post.num_comments} </a> </p>
                    </div>
                </div>`;

			newPost.innerHTML=newPostContent;
			fragment.append(newPost);
		}

        blogs.setAttribute('class','posts');
        blogs.append(fragment);
	})
    .then(()=>{
        if(HighlightPattern!=undefined){
            HighlightPattern();
        }
    })
	.catch((err) => console.log(err));
}


// Will respond for changes in Search Box
searchPost.oninput=async ()=>{
    const pattern=searchPost.value;
    //console.log(pattern);
   
    const queryUrl=`https://hn.algolia.com/api/v1/search?query=${pattern}&tags=story`
    const a=await fetchPosts(queryUrl,HighlightPattern);
}



/* MENUBAR : search [] by for []*/

// 1. Search Post

const sortPost=document.querySelector('#search');

sortPost.addEventListener('click',function(event){
    const pattern=event.currentTarget.value;
    console.log(pattern);
})


// 2. Search By
const sortBy=document.querySelector('#search-by');

const sortByDate='https://hn.algolia.com/api/v1/search_by_date?tags=story'

sortBy.addEventListener('click',function(event){
    
    if(event.currentTarget.value==='date'){
        fetchPosts(sortByDate);
    }else{
        fetchPosts(urlFrontPage);
    }
})


// 3. Search For
const sortFor=document.querySelector('#search-for');

sortFor.addEventListener('click',function(event){
    
    // api doesn't works hence leaving it
    let time='http://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i> ,created_at_i< ';
    
    // const days=event.currentTarget.value;
    // if(days==='custom-range'){

    // }else{
    //     const msOneDay=days*24*60*60;
    //     //const today=new Date();
    //     //const getDate=today.setTime(today.getTime()-(days*msOneDay));
    //   //  console.log(getDate);
    // 
    // }
})


// Page Navigation
const pageBtn=document.querySelectorAll('button').forEach(btn=>{
    btn.addEventListener('click',function(event){
        
        const toPage=`https://hn.algolia.com/api/v1/search/?page=${event.currentTarget.value}`;
        
        //window.location=toPage;
        //console.log(toPage);

        if(event.currentTarget.value==='1'){
            fetchPosts(urlFrontPage);
        }else if(event.currentTarget.value<=5){
            fetchPosts(toPage);
        }
    })
});

